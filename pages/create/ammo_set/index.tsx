import * as React from 'react';
import dynamic from 'next/dynamic';
const CreatePageComponent = dynamic(() => import("./modules/page"), {ssr: false});

export default class CreatePage extends React.Component {
  render() {
    return (<CreatePageComponent/>)
  }
}