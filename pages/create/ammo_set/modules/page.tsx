import * as React from 'react';
import {Button, Container, Menu, MenuItem, TextField} from "@material-ui/core";
import localDB from "../../../../services/local.db";
import PopupState, {bindMenu, bindTrigger} from 'material-ui-popup-state';
import {Formik} from "formik";
import * as yup from 'yup';

export default class CreatePageComponent extends React.Component {
  private schema = yup.object().shape({
    title: yup.string().required('Введите название'),
    price: yup.number().required(),
    exporter: yup.string().required(),
  });

  render() {
    let ammo = localDB.select('ammo');
    ammo = ammo ? ammo : [];
    let exporters = localDB.select('exporter');
    exporters = exporters ? exporters : [];
    return (
      <Container>
        <div style={{display: 'flex', flexDirection: 'column'}}>
          {ammo.map((a: any) =>
            <div>
              {a ? (
                <div>
                  <span>{a.title}</span>
                  <span>{a.price}</span>
                  <span>{a.exporter}</span>
                </div>
              ) : null}
            </div>
          )}
        </div>
        <Formik
          onSubmit={(values) => {
            localDB.insert('ammo', values);
            this.forceUpdate();
          }}
          initialValues={{
            title: undefined,
            price: undefined,
            exporter: undefined,
          }}
          validationSchema={this.schema}
          render={({
             handleSubmit,
             values,
             isValid,
             handleChange,
             handleBlur,
             setFieldValue
          }) => {
            return (
              <form onSubmit={handleSubmit} style={{display: 'flex', flexDirection: 'column'}}>
                <h1>Создание снаряжения</h1>
                <TextField
                  value={values.title}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="title"
                  label={'Название'}
                  style={{marginBottom: 30, maxWidth: 300}}
                  required
                />
                <PopupState variant="popover" popupId="demo-popup-menu">
                  {popupState => (
                    <React.Fragment>
                      <Button variant="contained" {...bindTrigger(popupState)} style={{maxWidth: 300}}>
                        {values.exporter ? values.exporter : 'Выбрать поставщика'}
                      </Button>
                      <Menu {...bindMenu(popupState)}>
                        {exporters.map((e: string) => (
                          <MenuItem onClick={() => {
                            setFieldValue('exporter', e);
                            popupState.close();
                          }}>{e}</MenuItem>
                        ))}
                      </Menu>
                    </React.Fragment>
                  )}
                </PopupState>
                <TextField
                  value={values.price}
                  label={'Цена'}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  name="price"
                  style={{marginBottom: 30, marginTop: 10, maxWidth: 300}}
                  required
                />
                <Button
                  type="submit"
                  disabled={!isValid}
                  variant="contained"
                  color="primary"
                  style={{maxWidth: 300}}
                >Создать</Button>
              </form>
            )
          }}
        />
      </Container>
    )
  }
}