import * as React from 'react';
import {Container, Button} from "@material-ui/core";
import {CSSProperties} from "react";

export default class CreatePage extends React.Component {
  render(){
    return(
      <Container style={container}>
        <h1>Выберите тип создаваемого объекта</h1>
        <Button style={button} href={'/create/ammo'} variant="contained" color="primary">Снаряжение</Button>
        <Button style={button} href={'/create/exporter'} variant="contained" color="primary">Поставщик</Button>
      </Container>
    )
  }
}

const container: CSSProperties = {
  display: 'flex',
  flexDirection: 'column',
  maxWidth: 800,
  margin: 'auto',
  alignItems: 'center'
};

const button: CSSProperties = {
  maxWidth: 400,
  marginTop: 20,
  minWidth: 300,
};