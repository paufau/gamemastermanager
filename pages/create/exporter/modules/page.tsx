import * as React from 'react';
import {Button, Container, TextField} from "@material-ui/core";
import localDB from "../../../../services/local.db";

interface IState {
  name?: string;
}

export default class CreateExporterComponent extends React.Component<{}, IState> {
  public state = {
    name: undefined,
  };

  render() {
    let exporters = localDB.select('exporter');
    exporters = exporters ? exporters : [];
    return (
      <Container style={{display: 'flex', flexDirection: 'column'}}>
        <div style={{display: 'flex', flexDirection: 'column'}}>
          {exporters.map(a =>
            <span>{a}</span>
          )}
        </div>
        <h1>Создание экспортера</h1>
        <TextField
          value={this.state.name}
          label={'Название'}
          onChange={(e) => {
            this.setState({name: e.target.value});
          }}
          style={{marginBottom: 20, maxWidth: 300}}
          required
        />
        <Button
          onClick={() => {
            localDB.insert('exporter', this.state.name);
            this.forceUpdate();
          }}
          disabled={this.state.name && Boolean(!this.state.name.length)}
          variant="contained"
          color="primary"
          style={{
            maxWidth: 300
          }}
        >Создать</Button>
      </Container>
    )
  }
}