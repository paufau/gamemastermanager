import * as React from 'react';
import Button from '@material-ui/core/Button';

export default class HomePage extends React.Component {
  public render(){
    return (
      <div>
        <h1>Hello my dear developer</h1>
        <Button href={'/create'} variant="contained" color="primary">
          Создание штук
        </Button>
      </div>
    )
  }
}