import {isNullOrUndefined} from "util";


class localDB {
  public static insert<T>(table: string, data: T[] | T){
    const serializedObject = localStorage.getItem(table);
    if (!serializedObject) {
      if (data instanceof Array){
        localStorage.setItem(table, JSON.stringify([...data]));
      } else {
        localStorage.setItem(table, JSON.stringify([data]));
      }
      return;
    }
    const currentData = JSON.parse(serializedObject);
    if (data instanceof Array){
      localStorage.setItem(table, JSON.stringify([...currentData, ...data]));
    } else {
      localStorage.setItem(table, JSON.stringify([...currentData, data]));
    }
  }

  public static select<T>(table: string, condition?: (item: T) => boolean): T[] {
    const serializedObject = localStorage.getItem(table);
    if (!serializedObject) {
      console.warn(`Cant't select from undefined table '${table}'`);
      return undefined;
    }
    if (condition) {
      return JSON.parse(serializedObject).filter(condition);
    }
    return JSON.parse(serializedObject);
  };
}

export default localDB;
