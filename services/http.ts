import localDB from "./local.db";

const develop = true;

class Http {
  instance = undefined;

  public async get<T>(url: string): Promise<T>{
    if (develop) {
      // @ts-ignore
      return localDB.select(url);
    }
  };

  public async post<T>(url: string, data: any): Promise<T>{
    return null;
  }

  constructor(){

  }
}

export default new Http()